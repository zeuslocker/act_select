# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :act_select,
  ecto_repos: [ActSelect.Repo]

# Configures the endpoint
config :act_select, ActSelectWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: System.get_env("SECRET_KEY_BASE"),
  render_errors: [view: ActSelectWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: ActSelect.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

config :facebook,
  app_id: System.get_env("FB_APP_ID"),
  app_secret: System.get_env("FB_APP_SECRET"),
  graph_url: "https://graph.facebook.com",
  graph_video_url: "https://graph-video.facebook.com"

config :act_select, ActSelect.Guardian,
  issuer: "act_select",
  allowed_drift: 2000,
  secret_key: System.get_env("SECRET_KEY_BASE"),
  token_ttl: %{
    "refresh" => {120, :days},
    "access" => {System.get_env("SESSION_TTL_HOURS") |> String.to_integer(), :hours}
  }

config :guardian, Guardian.DB,
  repo: ActSelect.Repo,
  schema_name: "guardian_tokens",
  # store all token types if not set
  token_types: ["refresh"],
  sweep_interval: 60

config :arc,
  storage: Arc.Storage.Local

config :rummage_ecto, Rummage.Ecto, repo: ActSelect.Repo
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
