defmodule ActSelect.UserActsTest do
  use ActSelect.DataCase

  alias ActSelect.UserActs

  describe "acts" do
    alias ActSelect.UserActs.Act

    @valid_attrs %{name: "some name"}
    @update_attrs %{name: "some updated name"}
    @invalid_attrs %{name: nil}

    def act_fixture(attrs \\ %{}) do
      {:ok, act} =
        attrs
        |> Enum.into(@valid_attrs)
        |> UserActs.create_act()

      act
    end

    test "list_acts/0 returns all acts" do
      act = act_fixture()
      assert UserActs.list_acts() == [act]
    end

    test "get_act!/1 returns the act with given id" do
      act = act_fixture()
      assert UserActs.get_act!(act.id) == act
    end

    test "create_act/1 with valid data creates a act" do
      assert {:ok, %Act{} = act} = UserActs.create_act(@valid_attrs)
      assert act.name == "some name"
    end

    test "create_act/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = UserActs.create_act(@invalid_attrs)
    end

    test "update_act/2 with valid data updates the act" do
      act = act_fixture()
      assert {:ok, act} = UserActs.update_act(act, @update_attrs)
      assert %Act{} = act
      assert act.name == "some updated name"
    end

    test "update_act/2 with invalid data returns error changeset" do
      act = act_fixture()
      assert {:error, %Ecto.Changeset{}} = UserActs.update_act(act, @invalid_attrs)
      assert act == UserActs.get_act!(act.id)
    end

    test "delete_act/1 deletes the act" do
      act = act_fixture()
      assert {:ok, %Act{}} = UserActs.delete_act(act)
      assert_raise Ecto.NoResultsError, fn -> UserActs.get_act!(act.id) end
    end

    test "change_act/1 returns a act changeset" do
      act = act_fixture()
      assert %Ecto.Changeset{} = UserActs.change_act(act)
    end
  end

  describe "users_acts" do
    alias ActSelect.UserActs.UserAct

    @valid_attrs %{}
    @update_attrs %{}
    @invalid_attrs %{}

    def user_act_fixture(attrs \\ %{}) do
      {:ok, user_act} =
        attrs
        |> Enum.into(@valid_attrs)
        |> UserActs.create_user_act()

      user_act
    end

    test "list_users_acts/0 returns all users_acts" do
      user_act = user_act_fixture()
      assert UserActs.list_users_acts() == [user_act]
    end

    test "get_user_act!/1 returns the user_act with given id" do
      user_act = user_act_fixture()
      assert UserActs.get_user_act!(user_act.id) == user_act
    end

    test "create_user_act/1 with valid data creates a user_act" do
      assert {:ok, %UserAct{} = user_act} = UserActs.create_user_act(@valid_attrs)
    end

    test "create_user_act/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = UserActs.create_user_act(@invalid_attrs)
    end

    test "update_user_act/2 with valid data updates the user_act" do
      user_act = user_act_fixture()
      assert {:ok, user_act} = UserActs.update_user_act(user_act, @update_attrs)
      assert %UserAct{} = user_act
    end

    test "update_user_act/2 with invalid data returns error changeset" do
      user_act = user_act_fixture()
      assert {:error, %Ecto.Changeset{}} = UserActs.update_user_act(user_act, @invalid_attrs)
      assert user_act == UserActs.get_user_act!(user_act.id)
    end

    test "delete_user_act/1 deletes the user_act" do
      user_act = user_act_fixture()
      assert {:ok, %UserAct{}} = UserActs.delete_user_act(user_act)
      assert_raise Ecto.NoResultsError, fn -> UserActs.get_user_act!(user_act.id) end
    end

    test "change_user_act/1 returns a user_act changeset" do
      user_act = user_act_fixture()
      assert %Ecto.Changeset{} = UserActs.change_user_act(user_act)
    end
  end
end
