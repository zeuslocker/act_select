defmodule ActSelect.AccountsTest do
  use ActSelect.DataCase

  alias ActSelect.Accounts

  describe "users" do
    alias ActSelect.Accounts.User

    @create_birthday ~D[1990-04-25]
    @update_birthday ~D[1960-03-15]
    @valid_attrs %{
      birthday: @create_birthday,
      gender: :male,
      fb_id: "1",
      bio: "some bio",
      fb_access_token: "some fb_access_token",
      first_name: "some first_name",
      phone: "some phone"
    }
    @update_attrs %{
      birthday: @update_birthday,
      gender: :female,
      fb_id: "2",
      bio: "some updated bio",
      fb_access_token: "some updated fb_access_token",
      first_name: "some updated first_name",
      phone: "some updated phone"
    }
    @invalid_attrs %{birthday: nil, bio: nil, fb_access_token: nil, first_name: nil, phone: nil}

    def user_fixture(attrs \\ %{}) do
      {:ok, user} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Accounts.create_user()

      user
    end

    test "list_users/0 returns all users" do
      user = user_fixture()
      assert Accounts.list_users() == [user]
    end

    test "get_user!/1 returns the user with given id" do
      user = user_fixture()
      assert Accounts.get_user!(user.id) == user
    end

    test "create_user/1 with valid data creates a user" do
      assert {:ok, %User{} = user} = Accounts.create_user(@valid_attrs)
      assert user.birthday == @create_birthday
      assert user.bio == "some bio"
      assert user.fb_access_token == "some fb_access_token"
      assert user.first_name == "some first_name"
      assert user.phone == "some phone"
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Accounts.create_user(@invalid_attrs)
    end

    test "update_user/2 with valid data updates the user" do
      user = user_fixture()
      assert {:ok, user} = Accounts.update_user(user, @update_attrs)
      assert %User{} = user
      assert user.birthday == @update_birthday
      assert user.bio == "some updated bio"
      assert user.fb_access_token == "some updated fb_access_token"
      assert user.first_name == "some updated first_name"
      assert user.phone == "some updated phone"
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = user_fixture()
      assert {:error, %Ecto.Changeset{}} = Accounts.update_user(user, @invalid_attrs)
      assert user == Accounts.get_user!(user.id)
    end

    test "delete_user/1 deletes the user" do
      user = user_fixture()
      assert {:ok, %User{}} = Accounts.delete_user(user)
      assert_raise Ecto.NoResultsError, fn -> Accounts.get_user!(user.id) end
    end

    test "change_user/1 returns a user changeset" do
      user = user_fixture()
      assert %Ecto.Changeset{} = Accounts.change_user(user)
    end
  end
end
