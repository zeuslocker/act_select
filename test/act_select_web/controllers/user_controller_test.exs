defmodule ActSelectWeb.UserControllerTest do
  use ActSelectWeb.ConnCase

  alias ActSelect.Accounts
  alias ActSelect.Accounts.User

  @create_birthday ~D[1990-04-25]
  @update_birthday ~D[1960-03-15]
  @create_attrs %{
    fb_id: "1",
    gender: :male,
    bio: "some bio",
    birthday: @create_birthday,
    fb_access_token: "some fb_access_token",
    first_name: "some first_name",
    phone: "some phone"
  }
  @update_attrs %{
    fb_id: "2",
    gender: :female,
    bio: "some updated bio",
    birthday: @update_birthday,
    fb_access_token: "some updated fb_access_token",
    first_name: "some updated first_name",
    phone: "some updated phone"
  }
  @invalid_attrs %{bio: nil, fb_access_token: nil, first_name: nil, phone: nil, birthday: nil}

  def fixture(:user) do
    {:ok, user} = Accounts.create_user(@create_attrs)
    user
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all users", %{conn: conn} do
      conn = get(conn, user_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create user" do
    test "renders user when data is valid", %{conn: conn} do
      conn = post(conn, user_path(conn, :create), user: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, user_path(conn, :show, id))

      assert json_response(conn, 200)["data"] == %{
               "id" => id,
               "birthday" => Date.to_string(@create_birthday),
               "bio" => "some bio",
               "fb_id" => "1",
               "first_name" => "some first_name",
               "gender" => "male"
             }
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, user_path(conn, :create), user: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update user" do
    setup [:create_user]

    test "renders user when data is valid", %{conn: conn, user: %User{id: id} = user} do
      conn = put(conn, user_path(conn, :update, user), user: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, user_path(conn, :show, id))

      assert json_response(conn, 200)["data"] == %{
               "id" => id,
               "birthday" => Date.to_string(@update_birthday),
               "fb_id" => "2",
               "bio" => "some updated bio",
               "first_name" => "some updated first_name",
               "gender" => "female"
             }
    end

    test "renders errors when data is invalid", %{conn: conn, user: user} do
      conn = put(conn, user_path(conn, :update, user), user: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete user" do
    setup [:create_user]

    test "deletes chosen user", %{conn: conn, user: user} do
      conn = delete(conn, user_path(conn, :delete, user))
      assert response(conn, 204)

      assert_error_sent(404, fn ->
        get(conn, user_path(conn, :show, user))
      end)
    end
  end

  defp create_user(_) do
    user = fixture(:user)
    {:ok, user: user}
  end
end
