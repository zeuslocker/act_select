defmodule ActSelect.Utils do
  @like_metacharacter_regex ~r{([\\%_])}
  @like_metacharacter_sanitize "\\\\\\1"

  @doc """
  Parse slashed date to NaiveDateTime

  ## Examples
    iex> ActSelect.Utils.parse_slashed_date("09/26/2019")
    ~D[2019-09-26]
  """
  def parse_slashed_date(date) do
    date |> Timex.parse!(Constants.slashed_date_format()) |> NaiveDateTime.to_date()
  end

  @doc """
  Allow to search strings containing `%_` characters

  ## Examples

    iex> ActSelect.Utils.like_sanitize("Vo%%Lo_d__ymyr%")
    ~S{Vo\\%\\%Lo\\_d\\_\\_ymyr\\%}
  """
  def like_sanitize(str) when is_binary(str) do
    String.replace(str, @like_metacharacter_regex, @like_metacharacter_sanitize)
  end

  @doc """
  Transform GraphQL search to Rummage search format

  ## Exapmles

    iex> [%{field: :first_name, value: "Volodymyr"}]
    ...> |> ActSelect.Utils.rummage_search_transform(:ilike)
    %{first_name: %{search_term: "%Volodymyr%", search_type: :ilike}}
  """
  def rummage_search_transform(search, search_type) when is_list(search) do
    search
    |> Enum.reduce(%{}, fn i, acc ->
      Map.put(
        acc,
        i.field,
        %{search_term: i.value, search_type: search_type}
      )
    end)
  end
end
