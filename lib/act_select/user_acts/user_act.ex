defmodule ActSelect.UserActs.UserAct do
  use Ecto.Schema
  import Ecto.Changeset
  alias ActSelect.Accounts.User
  alias ActSelect.Acts.Act
	alias ActSelect.UserActs.TimeInterval

  schema "users_acts" do
    belongs_to(:user, User)
    belongs_to(:act, Act)
		embeds_many(:active_intervals, TimeInterval)

    timestamps()
  end

  @doc false
  def changeset(user_act, attrs) do
    user_act
    |> cast(attrs, [:user_id, :act_id])
		|> cast_embed(:active_intervals)
    |> foreign_key_constraint(:act_id)
    |> foreign_key_constraint(:user_id)
    |> unique_constraint(:act,
      name: :users_acts_user_id_act_id_index,
      message: "has already been subscribed"
    )
  end
end
