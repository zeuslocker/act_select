defmodule ActSelect.UserActs do
  @moduledoc """
  The UserActs context.
  """

  import Ecto.Query, warn: false
  alias ActSelect.Repo

  alias ActSelect.Acts.Act
  alias ActSelect.Photo
  alias ActSelect.UserActs.UserAct
  alias ActSelect.Utils

  @doc """
  Data source for Absinthe dataloader.
  """
  def data do
    Dataloader.Ecto.new(Repo)
  end

  @doc """
  Returns the list of acts.

  ## Examples

      iex> list_acts()
      [%Act{}, ...]

  """
  def list_acts(selections, args) do
    query =
      case selections do
				[:subscribers_count, :active_intervals] ->
          Act
          |> join(:inner, [a], ua in assoc(a, :user_acts))
          |> select_merge([a, ua], %{subscribers_count: count(ua.id), active_intervals: ua.active_intervals})
          |> group_by([a, ua], [a.id, ua.active_intervals])
        [:subscribers_count] ->
          Act
          |> join(:inner, [a], ua in assoc(a, :user_acts))
          |> select([a, ua], %{a | subscribers_count: count(ua.id)})
          |> group_by([a, ua], [a.id])
        _ ->
          Act
      end

    query =
      case args do
        %{filter: %{name: name}} ->
          like_name = "%#{Utils.like_sanitize(name)}%"
          from(a in query, where: ilike(a.name, ^like_name))

        _ ->
          query
      end

    Repo.all(query)
  end

  @doc """
  Gets a single act.

  Raises `Ecto.NoResultsError` if the Act does not exist.

  ## Examples

      iex> get_act!(123)
      %Act{}

      iex> get_act!(456)
      ** (Ecto.NoResultsError)

  """
  def get_act!(id), do: Repo.get!(Act, id)

  @doc """
  Creates a act.

  ## Examples

      iex> create_act(%{field: value})
      {:ok, %Act{}}

      iex> create_act(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_act(attrs \\ %{}) do
    %Act{}
    |> Act.create_act_changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Subscribe/Unsubscribe user to act (delete or create UserAct).
  """
  def toggle_subscribe_act(%{act_id: act_id, user_id: user_id} = attrs) do
    changeset =
      %UserAct{}
      |> UserAct.changeset(attrs)

    Repo.transaction fn ->
      if Repo.get_by(UserAct, attrs) do
        from(ua in UserAct, where: ua.act_id == ^act_id and ua.user_id == ^user_id)
        |> Repo.delete_all()

        %{act_id: act_id, subscribed: false}
      else
        case Repo.insert(changeset) do
          {:ok, _} -> %{act_id: act_id, subscribed: true}
          {:error, changeset} -> Repo.rollback(changeset)
        end
      end
    end
  end

  @doc """
  Returns the list of act's users.
  """
  def act_users(act) do
    Ecto.assoc(act, :users) |> Repo.all()
  end

  @doc """
  Updates a act.

  ## Examples

      iex> update_act(act, %{field: new_value})
      {:ok, %Act{}}

      iex> update_act(act, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_act(%Act{} = act, attrs) do
    act
    |> Act.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Act.

  ## Examples

      iex> delete_act(act)
      {:ok, %Act{}}

      iex> delete_act(act)
      {:error, %Ecto.Changeset{}}

  """
  def delete_act(%Act{} = act) do
    Repo.delete(act)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking act changes.

  ## Examples

      iex> change_act(act)
      %Ecto.Changeset{source: %Act{}}

  """
  def change_act(%Act{} = act) do
    Act.changeset(act, %{})
  end

  alias ActSelect.UserActs.UserAct

  @doc """
  Returns the list of users_acts.

  ## Examples

      iex> list_users_acts()
      [%UserAct{}, ...]

  """
  def list_users_acts do
    Repo.all(UserAct)
  end

  @doc """
  Gets a single user_act.

  Raises `Ecto.NoResultsError` if the User act does not exist.

  ## Examples

      iex> get_user_act(123, 334)
      %UserAct{}

      iex> get_user_act(456, 34)
      ** (Ecto.NoResultsError)

  """
  def get_user_act(act_id, user_id, preloads \\ []) do
    user_act =
      Repo.one(
        from(ua in UserAct,
          where: ua.act_id == ^act_id and ua.user_id == ^user_id,
          preload: ^preloads
        )
      )

    case user_act do
      %UserAct{} -> {:ok, user_act}
      nil -> {:error, "Not found"}
    end
  end

  @doc """
  Creates a user_act.

  ## Examples

      iex> create_user_act(%{field: value})
      {:ok, %UserAct{}}

      iex> create_user_act(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_user_act(attrs \\ %{}) do
    %UserAct{}
    |> UserAct.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a user_act.

  ## Examples

      iex> update_user_act(user_act, %{field: new_value})
      {:ok, %UserAct{}}

      iex> update_user_act(user_act, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_user_act(%UserAct{} = user_act, attrs) do
    user_act
    |> UserAct.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a UserAct and ofphaned acts.

  ## Examples

      iex> delete_user_act(user_act)
      {:ok, %UserAct{}}

      iex> delete_user_act(user_act)
      {:error, %Ecto.Changeset{}}

  """
  def delete_user_act(act_id, user_id, preload_fields \\ []) do
    with {:ok, user_act} <- get_user_act(act_id, user_id, preload_fields) do
      with {:ok, _} <-
             Repo.transaction(fn ->
               Repo.delete!(user_act)

               Repo.delete_all(
                 from(a in Act,
                   where: fragment("? NOT IN (SELECT act_id FROM users_acts)", a.id)
                 )
               )
             end) do
        {:ok, user_act}
      end
    end
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking user_act changes.

  ## Examples

      iex> change_user_act(user_act)
      %Ecto.Changeset{source: %UserAct{}}

  """
  def change_user_act(%UserAct{} = user_act) do
    UserAct.changeset(user_act, %{})
  end
end
