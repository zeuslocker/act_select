defmodule ActSelect.UserActs.TimeInterval do
	use Ecto.Schema
	import Ecto.Changeset

	embedded_schema do
		field(:start, :time)
		field(:end, :time)
		field(:repeat, RepeatEnum)
		field(:every, :integer)
		field(:week_days, {:array, WeekDayEnum})
	end

	@doc false
	def changeset(time_interval, attrs) do
		time_interval
		|> cast(attrs, [:start, :end, :repeat, :every, :week_days])
		|> validate_required([:start, :end, :repeat])
		|> validate_number(:start, less_than: attrs[:end])
		|> validate_repeat()
		|> validate_number(:every, greater_than: 0)
	end

	def validate_repeat(changeset) do
		repeat = changeset.changes[:repeat]
		case repeat do
			:weekly -> validate_required(changeset, [:every, :week_days])
			:monthly -> validate_required(changeset, [:every])
		end
	end
end
