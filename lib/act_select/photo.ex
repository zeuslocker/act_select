defmodule ActSelect.Photo do
  use Ecto.Schema
  use Arc.Ecto.Schema

  import Ecto.Changeset

  schema "abstract table: photos" do
    field(:image, ActSelect.PhotoUploader.Type)
    field(:assoc_id, :id)

    timestamps()
  end

  @doc false
  def changeset(image, attrs) do
    image
    |> cast(attrs, [:image, :assoc_id])
    |> validate_required([:image])
  end
end
