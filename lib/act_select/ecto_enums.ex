import EctoEnum

defenum(GenderEnum, male: 0, female: 1)
defenum(RepeatEnum, never: 0, daily: 1, weekly: 2, monthly: 3)
defenum(WeekDayEnum, sun: 0, mon: 1, tue: 2, wed: 3, thu: 4, fri: 5, sat: 6)
