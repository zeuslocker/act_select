defmodule ActSelect.ApiWrappers.Facebook do
  @api_base "https://graph.facebook.com"

  @doc """
  Returns album id of given album type
  """
  def album_id_by_type(access_token, type) do
    HTTPoison.get(@api_base <> "/me/albums?fields=type" <> "&access_token=#{access_token}")
    |> Facebook.ResponseFormatter.format_response()
    |> Poison.decode!()
    |> Map.get("data")
    |> Enum.find(fn album -> album["type"] == type end)
    |> Map.get("id")
  end

  @doc """
  Fetches album photos
  """
  def album_photos(access_token, album_id, max_count) do
    HTTPoison.get(
      @api_base <>
        "/#{album_id}/photos?fields=images" <>
        "&limit=#{max_count}" <> "&access_token=#{access_token}"
    )
    |> Facebook.ResponseFormatter.format_response()
    |> Poison.decode!()
    |> Map.get("data")
    |> Enum.map(fn photo -> photo["images"] |> Enum.at(0) |> Map.get("source") end)
  end
end
