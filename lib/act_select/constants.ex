defmodule Constants do
  @slashed_date_format "{M}/{D}/{YYYY}"
  def slashed_date_format, do: @slashed_date_format
end
