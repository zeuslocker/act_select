defmodule ActSelect.Acts.Act do
  use Ecto.Schema
  import Ecto.Changeset

  alias ActSelect.UserActs.UserAct
  alias ActSelect.Accounts.User
  alias ActSelect.Photo

  schema "acts" do
    field(:name, :string)
    field(:description, :string)

    # user act view
    field(:subscribers_count, :integer, virtual: true)
    field(:active_intervals, {:array, :map}, virtual: true)
    #

    has_many(:photos, {"acts_photos", Photo}, foreign_key: :assoc_id)
    many_to_many(:users, User, join_through: UserAct)
    has_many(:user_acts, UserAct)

    timestamps()
  end

  @doc false
  def changeset(act, attrs) do
    act
    |> cast(attrs, [:name, :description])
    |> validate_required([:name, :description])
    |> unique_constraint(:name)
  end

  def create_act_changeset(user_act, attrs) do
    user_act
    |> changeset(attrs)
    |> Ecto.Changeset.cast_assoc(:photos, with: &Photo.changeset/2)
    |> Ecto.Changeset.cast_assoc(:user_acts, with: &UserAct.changeset/2)
  end
end
