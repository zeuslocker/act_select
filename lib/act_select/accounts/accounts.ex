defmodule ActSelect.Accounts do
  use Timex

  @moduledoc """
  The Accounts context.
  """

  import Ecto.Query, warn: false
  alias ActSelect.Repo
  alias ActSelect.Guardian
  alias ActSelect.Accounts.User
  alias ActSelect.ApiWrappers.Facebook, as: FacebookAPI
  alias ActSelect.Photo
  alias ActSelect.Utils

  @doc """
  Data source for Absinthe dataloader.
  """
  def data do
    Dataloader.Ecto.new(Repo)
  end

  @doc """
  Returns the list of users.

  ## Examples

      iex> list_users()
      [%User{}, ...]

  """
  def list_users(args) do
    args =
      if Map.has_key?(args, :search) do
        %{args | search: Utils.rummage_search_transform(args.search, :ilike)}
      else
        args
      end

    cond do
      Map.keys(args) |> Enum.any?(fn k -> k in [:paginate, :search, :sort] end) ->
        {queryable, rummage} = Rummage.Ecto.rummage(User, args, repo: Repo)
        {:ok, %{data: Repo.all(queryable), page_info: rummage.paginate}}

      true ->
        Repo.all(User)
    end
  end

  @doc """
  Gets a single user.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_user!(123)
      %User{}

      iex> get_user!(456)
      ** (Ecto.NoResultsError)

  """
  def get_user!(id), do: Repo.get!(User, id)

  @doc """
  Creates a user.

  ## Examples

      iex> create_user(%{field: value})
      {:ok, %User{}}

      iex> create_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_user(user_params) do
    %User{}
    |> User.changeset(user_params)
    |> Ecto.Changeset.cast_assoc(:photos, with: &Photo.changeset/2)
    |> Repo.insert()
  end

  @doc """
    Logins a user thru facebook.

    Fetches all necessary farams from Facebook, swap fb_access_token to long term token
    Either finds existing user by fb_id or creates new one.
  """
  def fb_create_session(fb_access_token) do
    with {:ok, user_params} <- user_params_from_fb(fb_access_token) do
      user = create_user(user_params) |> handle_existing_user()

      {:ok, access_token, access_claims} =
        Guardian.encode_and_sign(
          user,
          %{},
          token_type: "access"
        )

      {:ok, refresh_token, _claims} =
        Guardian.encode_and_sign(
          user,
          %{},
          token_type: "refresh"
        )

      {:ok,
       %{
         access_token: access_token,
         access_exp: access_claims["exp"],
         refresh_token: refresh_token
       }}
    end
  end

  defp user_params_from_fb(fb_access_token) do
    with {:ok, fb_params} <- Facebook.me("first_name,id,gender,birthday,email", fb_access_token) do
      {:ok, %{"access_token" => long_term_token}} =
        Facebook.long_lived_access_token(
          Application.get_env(:facebook, :app_id),
          Application.get_env(:facebook, :app_secret),
          fb_access_token
        )

      {:ok,
       %{
         first_name: fb_params["first_name"],
         fb_id: fb_params["id"],
         fb_access_token: long_term_token,
         gender: fb_params["gender"] |> String.to_atom(),
         birthday: fb_params["birthday"] |> Utils.parse_slashed_date(),
         email: fb_params["email"],
         photos: fb_user_photos(long_term_token)
       }}
    end
  end

  defp fb_user_photos(fb_access_token) do
    album_id = FacebookAPI.album_id_by_type(fb_access_token, "profile")

    FacebookAPI.album_photos(fb_access_token, album_id, 1)
    |> Enum.map(&%{image: &1})
  end

  defp handle_existing_user({:ok, user}), do: user

  defp handle_existing_user({:error, changeset}) do
    Repo.get_by!(User, fb_id: changeset.changes.fb_id)
  end

  @doc """
  Generages new access/refresh token pair, revokes old refresh token
  """
  def refresh_session(refresh_token) do
    with {:ok, {_old_token, _old_claims}, {access_token, access_claims}} <-
           Guardian.exchange(refresh_token, "refresh", "access") do
      {:ok, {_old_token, _old_claims}, {refresh_token, _refresh_claims}} =
        Guardian.refresh(refresh_token)

      {:ok,
       %{
         access_token: access_token,
         access_exp: access_claims["exp"],
         refresh_token: refresh_token
       }}
    end
  end

  @doc """
  Updates a user.

  ## Examples

      iex> update_user(user, %{field: new_value})
      {:ok, %User{}}

      iex> update_user(user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_user(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Returns user photos
  """
  def photos(user) do
    user |> Ecto.assoc(:photos) |> Repo.all()
  end

  @doc """
  Deletes a User.

  ## Examples

      iex> delete_user(user)
      {:ok, %User{}}

      iex> delete_user(user)
      {:error, %Ecto.Changeset{}}

  """
  def delete_user(%User{} = user) do
    Repo.delete(user)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking user changes.

  ## Examples

      iex> change_user(user)
      %Ecto.Changeset{source: %User{}}

  """
  def change_user(%User{} = user) do
    User.changeset(user, %{})
  end
end
