defmodule ActSelect.Accounts.User do
  use Ecto.Schema

  import Ecto.Changeset

  alias ActSelect.Photo
  alias ActSelect.Acts.Act
  alias ActSelect.UserActs.UserAct

  schema "users" do
    field(:birthday, :date)
    field(:bio, :string)
    field(:fb_access_token, :string)
    field(:fb_id, :string)
    field(:first_name, :string)
    field(:phone, :string)
    field(:gender, GenderEnum)
    field(:email, :string)

    has_many(:photos, {"users_photos", Photo}, foreign_key: :assoc_id)
    many_to_many(:acts, Act, join_through: UserAct)

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [
      :first_name,
      :bio,
      :birthday,
      :gender,
      :fb_id,
      :fb_access_token,
      :phone,
      :email
    ])
    |> validate_required([:first_name, :birthday, :gender, :fb_id])
    |> unique_constraint(:fb_id)
    |> unique_constraint(:phone)
  end
end
