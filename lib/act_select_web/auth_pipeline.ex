defmodule ActSelect.Guardian.AuthPipeline do
  use Guardian.Plug.Pipeline,
    otp_app: :act_select,
    module: ActSelect.Guardian,
    error_handler: ActSelect.AuthErrorHandler

  plug(Guardian.Plug.VerifyHeader, realm: :none, claims: %{typ: "access"})
  plug(Guardian.Plug.EnsureAuthenticated)
  plug(Guardian.Plug.LoadResource)
end
