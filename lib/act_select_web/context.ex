defmodule ActSelectWeb.Context do
  @behaviour Plug

  import Plug.Conn
  alias ActSelect.Guardian

  def init(opts), do: opts

  def call(conn, _) do
    token = Enum.at(get_req_header(conn, "authorization"), 0)

    case Guardian.resource_from_token(token, %{"typ" => "access"}) do
      {:ok, current_user, _claims} ->
        Absinthe.Plug.put_options(conn, context: %{current_user: current_user})

      {:error, _reason} ->
        conn
    end
  end
end
