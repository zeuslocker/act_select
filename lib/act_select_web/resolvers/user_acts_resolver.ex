defmodule ActSelectWeb.Resolvers.UserActsResolver do
  alias ActSelect.UserActs

  import ActSelectWeb.Schema.Helpers, only: [changeset_errors: 1, filter_selection_preloads: 2]

  def list_acts(_, args, info) do
    {:ok, filter_selection_preloads(info, ["subscribers_count", "active_intervals"]) |> UserActs.list_acts(args)}
  end

  def create_act(_parent, args, %{context: %{current_user: current_user}}) do
    photos = Enum.map(args.photos, fn ph -> %{image: ph} end)
    args = args
    |> Map.put(:user_acts, [%{user_id: current_user.id, active_intervals: args[:active_intervals] || []}])
    |> Map.put(:photos, photos)
    |> Map.delete(:active_intervals)

    case UserActs.create_act(args) do
      {:ok, act} -> {:ok, act}
      {:error, changeset} -> {:error, changeset_errors(changeset)}
    end
  end

  def toggle_subscribe_act(_parent, args, %{context: %{current_user: current_user}}) do
    args = args
    |> Map.put(:user_id, current_user.id)

    case UserActs.toggle_subscribe_act(args) do
      {:ok, act_subsctibe_result} -> {:ok, act_subsctibe_result}
      {:error, changeset} -> {:error, changeset_errors(changeset)}
    end
  end

  def act_users(act, _, _) do
    {:ok, UserActs.act_users(act)}
  end

  def delete_user_act(_, %{act_id: act_id}, info) do
    case UserActs.delete_user_act(
           act_id,
           info.context.current_user.id,
           filter_selection_preloads(info, ["user", "act"])
         ) do
      {:ok, user_act} -> {:ok, user_act}
      {:error, message} -> {:error, message}
    end
  end
end
