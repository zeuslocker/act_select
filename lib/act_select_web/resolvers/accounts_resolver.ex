defmodule ActSelectWeb.Resolvers.AccountsResolver do
  alias ActSelect.Accounts

  def me(_parent, _args, %{context: %{current_user: current_user}}) do
    {:ok, current_user}
  end

  def list_users(_parent, args, _context) do
    Accounts.list_users(args)
  end

  def refresh_session(_parent, %{refresh_token: token}, _context) do
    case Accounts.refresh_session(token) do
      {:ok, credentials} -> {:ok, credentials}
      {:error, _} -> {:error, "Refresh token invalid"}
    end
  end

  def fb_create_session(_parent, %{fb_access_token: token}, _context) do
    case Accounts.fb_create_session(token) do
      {:ok, credentials} -> {:ok, credentials}
      {:error, _error} -> {:error, "Access token invalid"}
    end
  end

  def photos(user, _args, _context) do
    {:ok, Accounts.photos(user)}
  end
end
