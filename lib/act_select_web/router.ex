defmodule ActSelectWeb.Router do
  use ActSelectWeb, :router

  #  pipeline :browser do
  #    plug(:accepts, ["html"])
  #    plug(:fetch_session)
  #    plug(:fetch_flash)
  #    plug(:protect_from_forgery)
  #    plug(:put_secure_browser_headers)
  #  end

  #  pipeline :api do
  #    plug(:accepts, ["json"])
  #  end

  pipeline :graphql do
    plug(ActSelectWeb.Context)
  end

  pipe_through([:graphql])

  forward("/graphiql", Absinthe.Plug.GraphiQL, schema: ActSelectWeb.Schema)
  forward("/graphql/v1", Absinthe.Plug, schema: ActSelectWeb.Schema)
end
