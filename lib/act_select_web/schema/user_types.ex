defmodule ActSelectWeb.Schema.UserTypes do
  use Absinthe.Schema.Notation

  import ActSelectWeb.Schema.Helpers, only: [safely: 1]
  import Absinthe.Resolution.Helpers, only: [dataloader: 1]

  alias ActSelectWeb.Resolvers.AccountsResolver
  alias ActSelect.Accounts

  enum :user_search_fields do
    value(:first_name)
    value(:age)
  end

  input_object :user_search do
    field(:field, non_null(:user_search_fields))
    field(:value, non_null(:string))
  end

  input_object :user_sort do
    field(:field, non_null(:user_search_fields))
    field(:order, :order, default_value: :asc)
  end

  object :paginated_user_list do
    field(:data, list_of(:user))
    field(:page_info, :page_info)
  end

  object :account_queries do
    field :me, :user do
      resolve(&AccountsResolver.me/3)
    end

    field :user_list, :paginated_user_list do
      arg(:search, list_of(:user_search))
      arg(:sort, :user_sort)
      arg(:paginate, :paginate)
      resolve(&AccountsResolver.list_users/3)
    end
  end

  object :account_mutations do
    field :fb_create_session, type: :credentials do
      arg(:fb_access_token, non_null(:string))

      resolve(safely(&AccountsResolver.fb_create_session/3))
    end

    field :refresh_session, type: :credentials do
      arg(:refresh_token, non_null(:string))

      resolve(safely(&AccountsResolver.refresh_session/3))
    end
  end

  object :credentials do
    field(:access_token, :string)
    field(:access_exp, :string)
    field(:refresh_token, :string)
  end

  object :user do
    field(:id, :id)
    field(:bio, :string)
    field(:first_name, :string)

    field(:photos, list_of(:photo), resolve: dataloader(Accounts))
    field(:acts, list_of(:act), resolve: dataloader(Accounts))
  end

  object :photo do
    field :image, :string do
      arg(:version, non_null(:version))

      resolve(fn photo, %{version: version}, _ ->
        {:ok, ActSelect.PhotoUploader.url({photo.image, photo}, version)}
      end)
    end
  end

  enum :version do
    value(:original)
    value(:thumb)
  end
end
