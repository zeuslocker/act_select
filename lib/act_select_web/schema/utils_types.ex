defmodule ActSelectWeb.Schema.UtilsTypes do
  use Absinthe.Schema.Notation

  object :page_info do
    field(:max_page, non_null(:integer))
    field(:page, non_null(:integer))
    field(:per_page, non_null(:integer))
    field(:total_count, non_null(:integer))
  end

  input_object :paginate do
    field(:page, non_null(:integer))
    field(:per_page, non_null(:integer))
  end

  enum :order do
    value(:asc)
    value(:desc)
  end
end
