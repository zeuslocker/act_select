defmodule ActSelectWeb.Schema.Helpers do
  use Absinthe.Schema.Notation

  def filter_selection_preloads(info, names) do
    info.definition.selections
    |> Enum.map(& &1.schema_node.name)
    |> Enum.filter(&(&1 in names))
    |> Enum.map(&String.to_atom(&1))
  end

  def safely(fun) do
    fn parent, args, resolution ->
      try do
        fun.(parent, args, resolution)
      rescue
        e -> {:error, Exception.message(e)}
      end
    end
  end

  @doc """
  Retrieve errors from changeset

  ## Examples

    iex> ActSelectWeb.Schema.Helpers.changeset_errors(changeset)
    ["Name already exists", "Age can't be blank"]
  """
  def changeset_errors(changeset) do
    Ecto.Changeset.traverse_errors(changeset, fn {msg, opts} ->
      Enum.reduce(opts, msg, fn {key, value}, acc ->
        String.replace(acc, "%{#{key}}", to_string(value))
      end)
    end)
    |> fetch_errors
  end

  defp fetch_errors(obj) do
    Enum.map(obj, fn {key, value} ->
      Enum.map(value, fn value ->
        cond do
          is_binary(value) ->
            field_name = to_string(key) |> String.capitalize
            field_name <> " " <> value
          is_map(value) -> fetch_errors(value) |> Enum.join(", ")
        end
      end) |> Enum.join()
     end)
  end
end
