defmodule ActSelectWeb.Schema.UserActTypes do
  use Absinthe.Schema.Notation

  import Absinthe.Resolution.Helpers, only: [dataloader: 1]
	import_types Absinthe.Type.Custom

  alias ActSelectWeb.Resolvers.UserActsResolver
  alias ActSelect.UserActs

  object :user_act_mutations do
    field :create_act, type: :act do
      arg(:name, non_null(:string))
      arg(:description, non_null(:string))
      arg(:photos, list_of(:upload))
			arg(:active_intervals, list_of(:time_interval_input))

      resolve(&UserActsResolver.create_act/3)
    end

    field :delete_user_act, type: :user_act do
      arg(:act_id, non_null(:id))

      resolve(&UserActsResolver.delete_user_act/3)
    end

    field :toggle_subscribe_act, type: :act_subscribe_result do
      arg(:act_id, non_null(:id))

      resolve(&UserActsResolver.toggle_subscribe_act/3)
    end
  end

  object :user_act_queries do
    field :act_list, type: list_of(:act) do
      arg(:filter, :act_filter)

      resolve(&UserActsResolver.list_acts/3)
    end
  end

  object :act do
    field(:id, non_null(:id))
    field(:name, non_null(:string))
    field(:description, non_null(:string))
    field(:photos, list_of(:photo), resolve: dataloader(UserActs))
    field(:subscribers, list_of(:user), resolve: dataloader(UserActs))
		field(:active_intervals, list_of(:time_interval))
    field(:subscribers_count, :integer)
  end

  object :user_act do
    field(:id, non_null(:id))
    field(:user, non_null(:user))
    field(:act, non_null(:act))
  end

  object :act_subscribe_result do
    field(:act_id, non_null(:id))
    field(:subscribed, non_null(:boolean))
  end

	enum :repeat_enum, values: [:never, :daily, :weekly, :monthly]
	enum :week_days_enum, values: [:sun, :mon, :tue, :wed, :thu, :fri, :sat]

	object :time_interval do
		field(:start, non_null(:time))
		field(:end, non_null(:time))
		field(:repeat, :repeat_enum)
		field(:week_days, list_of(:week_days_enum))
		field(:every, :integer)
	end

	input_object :time_interval_input do
		field(:start, non_null(:time))
		field(:end, non_null(:time))
		field(:repeat, :repeat_enum)
		field(:week_days, list_of(:week_days_enum))
		field(:every, :integer)
	end

  input_object :act_filter do
    field(:name, :string)
  end
end
