defmodule ActSelectWeb.Middlewares.Authentication do
  @behaviour Absinthe.Middleware

  def call(resolution, _) do
    case Map.fetch(resolution.context, :current_user) do
      {:ok, _user} ->
        resolution

      :error ->
        Absinthe.Resolution.put_result(resolution, {:error, "Access token invalid"})
    end
  end
end
