defmodule ActSelectWeb.Schema do
  use Absinthe.Schema
  @authorized_fields ~w(me create_act act_list delete_user_act subscribe_act)a

  import_types(Absinthe.Plug.Types)

  import_types(ActSelectWeb.Schema.{
    UserTypes,
    UserActTypes,
    UtilsTypes
  })

  query do
    import_fields(:account_queries)
    import_fields(:user_act_queries)
  end

  mutation do
    import_fields(:account_mutations)
    import_fields(:user_act_mutations)
  end

  def middleware(middleware, field, _obj) do
    if field.identifier in @authorized_fields do
      [ActSelectWeb.Middlewares.Authentication] ++ middleware
    else
      middleware
    end
  end

  def context(ctx) do
    loader =
      Dataloader.new()
      |> Dataloader.add_source(ActSelect.UserActs, ActSelect.UserActs.data())
      |> Dataloader.add_source(ActSelect.Accounts, ActSelect.Accounts.data())

    Map.put(ctx, :loader, loader)
  end

  def plugins do
    [Absinthe.Middleware.Dataloader] ++ Absinthe.Plugin.defaults()
  end
end
