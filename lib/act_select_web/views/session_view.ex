defmodule ActSelectWeb.SessionView do
  use ActSelectWeb, :view

  def render("refresh.json", assigns) do
    %{
      access_token: assigns.access_token,
      access_expired_at: assigns.access_expired_at,
      refresh_token: assigns.refresh_token
    }
  end

  def render("create.json", assigns) do
    %{
      access_token: assigns.access_token,
      access_expired_at: assigns.access_expired_at,
      refresh_token: assigns.refresh_token
    }
  end
end
