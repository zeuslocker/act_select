defmodule ActSelectWeb.UserView do
  use ActSelectWeb, :view
  alias ActSelectWeb.UserView

  def render("index.json", %{users: users}) do
    %{data: render_many(users, UserView, "user.json")}
  end

  def render("show.json", %{user: user}) do
    %{data: render_one(user, UserView, "user.json")}
  end

  def render("user.json", %{user: user}) do
    %{
      id: user.id,
      first_name: user.first_name,
      bio: user.bio,
      birthday: user.birthday,
      gender: user.gender
    }
  end
end
