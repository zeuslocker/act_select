defmodule ActSelect.Repo.Migrations.CreateUsersActs do
  use Ecto.Migration

  def change do
    create table(:users_acts) do
      add :user_id, references(:users, on_delete: :delete_all), null: false
      add :act_id, references(:acts, on_delete: :delete_all), null: false

      timestamps()
    end

    create index(:users_acts, [:user_id])
    create index(:users_acts, [:act_id])
    create unique_index(:users_acts, [:user_id, :act_id])
  end
end
