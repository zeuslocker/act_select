defmodule ActSelect.Repo.Migrations.AddDescriptionToActs do
  use Ecto.Migration

  def change do
    alter table(:acts) do
      add :description, :string, null: false
    end
  end
end
