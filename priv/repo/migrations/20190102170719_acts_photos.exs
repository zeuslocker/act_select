defmodule ActSelect.Repo.Migrations.ActsPhotos do
  use Ecto.Migration

  def change do
    create table(:acts_photos) do
      add :image, :string, null: false
      add :assoc_id, references(:acts, on_delete: :delete_all), null: false

      timestamps()
    end
  end
end
