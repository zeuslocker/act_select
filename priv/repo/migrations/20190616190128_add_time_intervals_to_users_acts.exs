defmodule ActSelect.Repo.Migrations.AddTimeIntervalsToUsersActs do
  use Ecto.Migration

  def change do
		alter table(:users_acts) do
			add :active_intervals, {:array, :map}, default: []
		end
  end
end
