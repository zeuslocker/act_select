defmodule ActSelect.Repo.Migrations.CreateActs do
  use Ecto.Migration

  def change do
    create table(:acts) do
      add :name, :string

      timestamps()
    end

    create unique_index(:acts, [:name])
  end
end
