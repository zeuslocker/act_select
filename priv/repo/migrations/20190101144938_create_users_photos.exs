defmodule ActSelect.Repo.Migrations.CreatePhotos do
  use Ecto.Migration

  def change do
    create table(:users_photos) do
      add :image, :string, null: false
      add :assoc_id, references(:users, on_delete: :delete_all), null: false

      timestamps()
    end
  end
end
