defmodule ActSelect.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :first_name, :string, null: false
      add :bio, :string
      add :birthday, :date, null: false
      add :gender, :integer, null: false
      add :fb_access_token, :string
      add :fb_id, :string, null: false
      add :phone, :string
      add :email, :string

      timestamps()
    end

    create unique_index(:users, [:fb_id])
    create unique_index(:users, [:phone])
    create unique_index(:users, [:email])
  end
end
